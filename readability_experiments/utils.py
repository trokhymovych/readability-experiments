import os
import re

import numpy as np
import torch


def set_seed(seed=42):
    """
    Sets the seed of the entire notebook so results are the same every time we run.
    This is for REPRODUCIBILITY.
    """
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    # When running on the CuDNN backend, two further options must be set
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    # Set a fixed value for the hash seed
    os.environ['PYTHONHASHSEED'] = str(seed)


def extract_language_code(file_name: str) -> str:
    # Define a regex pattern to match the language code separated by _
    pattern = re.compile(r'-(\w+)_')
    # Use the findall method to extract the language code
    matches = pattern.findall(file_name)
    # Check if there is a match and get the language code
    if matches:
        return matches[0]
    else:
        return None
