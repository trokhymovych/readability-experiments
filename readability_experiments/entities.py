import enum

from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class Difficulty(str, enum.Enum):
    easy = "easy"
    hard = "hard"


class WikiPage(BaseModel):
    title: str
    sentences: List[str]
    difficulty: Difficulty
    lang: str
    score: Optional[float] = None
    sentence_scores: Optional[List[float]] = None


class WikiDataset(BaseModel):
    files: List[str]
    data: Dict[str, List[Any]]
