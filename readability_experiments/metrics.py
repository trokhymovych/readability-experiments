import time

from copy import deepcopy
from typing import List, Tuple

import numpy as np
import pandas as pd

from tqdm import tqdm

from readability_experiments.constants import (
    ACCURACY_TABLE_PATH,
    METRICS_PATH,
    RANKING_ACCURACY_TABLE_PATH,
)
from readability_experiments.data_loader import WikiDataset
from readability_experiments.entities import Difficulty, WikiPage
from readability_experiments.models.base import ModelBase


ACCURACY = "accuracy"
PAIRWISE_ACCURACY = "pairwise_accuracy"


class Evaluator:
    """
    Class that implements the logic of unified metrics calculation and results saving
    """
    def __init__(self, models: List[ModelBase], salt: str = ""):
        self.models = models
        self.salt = salt

    def get_models_metrics(self, testset: WikiDataset):
        """
        Method that takes dataset as an input and calculate all
        possible metrics for the models available in the evaluator
        """
        for model in self.models:
            file_names, accuracy, pairwise_accuracy = [], [], []
            start_time = time.time()
            processed_data = model.predict(testset)
            time_elapsed = time.time() - start_time
            self.dump_model_predictions(processed_data, model.name, time_elapsed)
            for file in tqdm(testset.files):
                file_names.append(file)
                # Ranking metrics:
                pairwise_accuracy.append(self.pairwise_accuracy(processed_data.data[file]))
                # Classification metrics:
                if model.allows_classification:
                    accuracy.append(self.accuracy(processed_data.data[file]))
                else:
                    accuracy.append(None)
            print(f"Saving results for {model.name}")
            pd.DataFrame(
                {"file": file_names, ACCURACY: accuracy, PAIRWISE_ACCURACY: pairwise_accuracy}
            ).to_csv(METRICS_PATH + f"{self.salt}_{model.name.replace(' ', '_')}.csv", index=False)

    def dump_model_predictions(self, testset: WikiDataset, model_name, time_elapsed) -> None:
        """
        Method that dupms the predictions for the models available in the evaluator
        """
        testset_to_dump = deepcopy(testset)
        data_values = []
        # Setting the values of sentences to [] to save memory:
        for file in tqdm(testset_to_dump.files):
            for pair in testset_to_dump.data[file]:
                for sample in pair:
                    sample.sentences = []
                    data_values.append(sample.__dict__)
                    data_values[-1]["file"] = file
        pd.DataFrame(data_values).to_csv(
            METRICS_PATH + f"all_scores_{self.salt}_{model_name.replace(' ', '_')}_{time_elapsed}.csv", index=False
        )
        del testset_to_dump
        return testset

    def dump_model_metrics(self) -> None:
        """
        Method that takes the precalculated metrics and build markdowns to save them in the readme.
        """
        # Building accuracy Markdown table:
        metrics_dfs = []
        for model in self.models:
            model_df = pd.read_csv(METRICS_PATH + f"{self.salt}_{model.name.replace(' ', '_')}.csv")
            model_df.index = model_df.file
            model_df = model_df[ACCURACY]
            metrics_dfs.append(model_df)
        with open(ACCURACY_TABLE_PATH, 'w') as output_file:
            df_to_dump = pd.concat(metrics_dfs, axis=1)
            df_to_dump.columns = [model.name for model in self.models]
            output_file.write(df_to_dump.to_markdown())

        metrics_dfs = []
        for model in self.models:
            model_df = pd.read_csv(METRICS_PATH + f"{self.salt}_{model.name.replace(' ', '_')}.csv")
            model_df.index = model_df.file
            model_df = model_df[PAIRWISE_ACCURACY]
            metrics_dfs.append(model_df)
        with open(RANKING_ACCURACY_TABLE_PATH, 'w') as output_file:
            df_to_dump = pd.concat(metrics_dfs, axis=1)
            df_to_dump.columns = [model.name for model in self.models]
            output_file.write(df_to_dump.to_markdown())

    @staticmethod
    def accuracy(data: List[Tuple[WikiPage, WikiPage]], threshold: float = 0.5) -> float:
        """
        Function that calculates the accuracy score based on the scores and given threshold.
        :param threshold: value from 0 to 1 used for prediction
        :param data: testset to use for evaluation
        :return: accuracy
        """
        correct = []
        for pair in data:
            for sample in pair:
                prediction = sample.score > threshold
                true = False if sample.difficulty == Difficulty.easy else True
                correct.append(prediction == true)
        return np.sum(correct) / len(correct)

    @staticmethod
    def pairwise_accuracy(data: List[Tuple[WikiPage, WikiPage]]) -> float:
        """
        Function that calculates the accuracy score based on the scores and given threshold.
        :param data: testset to use for evaluation
        :return: pairwise accuracy
        """
        correct = []
        for pair in data:
            scores_list = [sample.score for sample in pair]
            # Check if list is ordered:
            if scores_list == sorted(scores_list):
                correct.append(True)
            else:
                correct.append(False)
        return np.sum(correct) / len(correct)
