from typing import Dict

import numpy as np
import pandas as pd
import torch

from datasets import ClassLabel, Dataset, load_metric
from tqdm import tqdm
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    Trainer,
    TrainingArguments,
    pipeline,
)

from readability_experiments.constants import (
    BASE_MODEL,
    MAX_LENGTH,
    MODELS_PATH,
    N_EPOCHS,
    RANDOM_SEED,
    SAMPLE_TEST_SIZE,
)
from readability_experiments.entities import Difficulty, WikiDataset
from readability_experiments.models.base import ModelBase


class TRClassifier(ModelBase):
    """
    Transformer based classifier model
    """

    name = "Transformer based classifier"
    allows_classification: bool = True

    def __init__(self, params: Dict = {}):
        super().__init__(params)
        # model initialization:
        self.model_checkpoint = params.get("initial_model", BASE_MODEL)
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_checkpoint)
        self.model = AutoModelForSequenceClassification.from_pretrained(self.model_checkpoint, num_labels=2)
        self.batch_size = params.get("batch_size", 10)
        self.classifier = None

    def fit(self, trainset: WikiDataset, **kwargs) -> None:
        """
        Method used for model training. Parameters are hardcoded.
        """
        encoded_dataset = self._prepare_data(trainset)
        args = TrainingArguments(
            f"{MODELS_PATH}bert_tuned/",
            evaluation_strategy="epoch",
            save_strategy="epoch",
            learning_rate=2e-5,
            per_device_train_batch_size=self.batch_size,
            per_device_eval_batch_size=self.batch_size,
            num_train_epochs=N_EPOCHS,
            weight_decay=0.01,
            load_best_model_at_end=True,
            metric_for_best_model="accuracy",
            push_to_hub=False,
        )

        metric = load_metric("glue", "mrpc")

        def compute_metrics(eval_pred):
            predictions, labels = eval_pred
            predictions = np.argmax(predictions, axis=1)
            return metric.compute(predictions=predictions, references=labels)

        trainer = Trainer(
            self.model,
            args,
            train_dataset=encoded_dataset["train"],
            eval_dataset=encoded_dataset["test"],
            tokenizer=self.tokenizer,
            compute_metrics=compute_metrics
        )

        trainer.train()
        self.model = trainer.model

    def predict(self, testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Method used for batch predict scores for the whole dataset
        """
        # Creating classification pipeline
        if torch.cuda.is_available():
            device = 0
        else:
            device = "cpu"

        classifier = pipeline(
            task="text-classification",
            model=self.model,
            tokenizer=self.tokenizer,
            device=device,
            batch_size=self.batch_size
        )

        # Preparing texts for prediction
        sentences = []
        sample_id = 0
        sample_ids = []
        sample_rank_id = []
        for file in testset.files:
            for sample_pair in testset.data[file]:
                for sample in sample_pair:
                    sentences += sample.sentences
                    sample_ids += [sample_id] * len(sample.sentences)
                    sample_id += 1
                    sample_rank_id += list(np.arange(1, len(sample.sentences) + 1))

        # Sorting sentences by the length to make the batch processing more efficient:
        texts_length = [len(t) for t in sentences]
        ids = np.argsort(texts_length)[::-1]
        sentences_sorted = [sentences[i] for i in ids]
        sample_ids_sorted = [sample_ids[i] for i in ids]
        sample_rank_id_sorted = [sample_rank_id[i] for i in ids]

        # Predicting using pipeline:
        tqdm_batch_size = self.batch_size * 10
        predicted_scores = []
        for i in tqdm(range(0, len(sentences_sorted), tqdm_batch_size)):
            # sorting texts for better performance:
            texts_batch = sentences_sorted[i:i + tqdm_batch_size]
            tokenizer_kwargs = {'truncation': True, 'max_length': MAX_LENGTH}
            predicted_scores += classifier(
                texts_batch,
                return_all_scores=True,
                **tokenizer_kwargs,
                batch_size=self.batch_size
            )

        # Aggregating scores:
        def predictions_processing(predictions):
            res = []
            for i in predictions:
                res.append(i[1]['score'])
            return res

        def get_scores(sample_results_df):
            # filling 'bert_score_mean', 'bert_score_max', 'bert_score_min'
            sentences_scores = sample_results_df["score"]
            final_score = 0
            if len(sentences_scores) > 0:
                final_score = float(np.mean(sentences_scores))
            result = pd.DataFrame(
                {
                    "sample_id": [sample_results_df.sample_id.values[0]],
                    "score": [final_score]
                }
            )
            return result

        tmp_df = pd.DataFrame({
            "sample_id": sample_ids_sorted,
            "sample_rank_id": sample_rank_id_sorted,
            "score": predictions_processing(predicted_scores)
        })
        tmp_df_scores = tmp_df.groupby('sample_id').apply(get_scores).reset_index(drop=True)
        tmp_df_scores.index = tmp_df_scores["sample_id"]
        sample_id_to_score = tmp_df_scores.to_dict("index")
        # Assigning scores to the testset
        sample_id = 0
        for file in testset.files:
            for sample_pair in testset.data[file]:
                for sample in sample_pair:
                    sample.score = sample_id_to_score.get(sample_id, {"score": -10})["score"]
                    sample_id += 1
        return testset

    def _prepare_data(self, trainset: WikiDataset):
        """
        Method that process the training dataset to use it for training
        :param trainset:
        :return:
        """
        # creating csv:
        data = []
        for file in trainset.files:
            for pair in trainset.data[file]:
                for el in pair:
                    for sentence in el.sentences:
                        if len(sentence) > 1 and not pd.isna(sentence):
                            data.append({"text": sentence, "label": el.difficulty.value})
        training_dataset = Dataset.from_list(data)
        feat_class = ClassLabel(num_classes=2, names=[Difficulty.easy.value, Difficulty.hard.value])
        print("Data size: ", len(data))
        training_dataset = training_dataset.cast_column("label", feat_class)
        training_dataset = training_dataset.train_test_split(
            test_size=SAMPLE_TEST_SIZE,
            stratify_by_column="label",
            shuffle=True,
            seed=RANDOM_SEED
        )

        def preprocess_function(examples):
            return self.tokenizer(examples["text"], truncation=True, max_length=MAX_LENGTH)

        encoded_dataset = training_dataset.map(preprocess_function, batched=True)
        return encoded_dataset
