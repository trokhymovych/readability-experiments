import textstat

from tqdm import tqdm

from readability_experiments.constants import (
    TEXTSTATS_DEFAULT_LANGUAGE,
    TEXTSTATS_SUPPORTED_LANGUAGES,
)
from readability_experiments.entities import WikiDataset
from readability_experiments.models import Baseline


class FKBaseline(Baseline):
    """
    Baseline model that predict the score based on English FK score
    """

    name = "English flesch_reading_ease baseline"
    allows_classification: bool = False

    @staticmethod
    def predict(testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Model used for prediction using trained model
        """
        for file in tqdm(testset.files):
            for pair in testset.data[file]:
                for sample in pair:
                    textstat.set_lang(
                        sample.lang if sample.lang in TEXTSTATS_SUPPORTED_LANGUAGES else TEXTSTATS_DEFAULT_LANGUAGE
                    )
                    # Using inverted score for pipeline consistency
                    sample.score = 1 / (textstat.flesch_reading_ease(" ".join(sample.sentences)) + 0.1)
        return testset
