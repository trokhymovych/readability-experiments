import pandas as pd

from catboost import CatBoostRanker, Pool
from tqdm import tqdm

from readability_experiments.entities import WikiDataset
from readability_experiments.models.lf_classifier import LFClassifier


class LFRanker(LFClassifier):
    """
    Baseline model based on Linguistic features + Catboost ranker
    """

    name = "Linguistic features ranker"
    allows_classification: bool = False

    def fit(self, trainset: WikiDataset, **kwargs) -> None:
        """
        Method used for model training
        Not needed in the baseline model
        """
        train_features, labels = self._prepare_features(trainset)
        # Building classifier model:
        train_data = Pool(
            data=pd.DataFrame(train_features),
            label=labels,
            group_id=[j for i in range(1, int(len(labels) / 2) + 1) for j in [i, i]]
        )
        self.model = CatBoostRanker(iterations=5000, metric_period=500, verbose=True, learning_rate=0.01)
        self.model.fit(train_data, plot=False)

    def predict(self, testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Model used for prediction using trained model
        """
        # Calculate test features:
        test_features, _ = self._prepare_features(testset, train_mode=False)
        # Calculate test scores:
        scores = self.model.predict(
            [[features_sample[k] for k in self.model.feature_names_] for features_sample in test_features]
        )

        # Assigning scores to testset samples:
        i = 0
        for file in tqdm(testset.files):
            for pair in testset.data[file]:
                for sample in pair:
                    sample.score = scores[i]
                    i += 1
        return testset
