from typing import Dict

import lftk
import pandas as pd
import spacy

from catboost import CatBoostClassifier, Pool
from tqdm import tqdm

from readability_experiments.constants import (
    FEATURES_BATCH_SIZE,
    SPACY_DEFAULT_MODEL,
    SPACY_DICT,
)
from readability_experiments.entities import WikiDataset
from readability_experiments.models.base import ModelBase


class LFClassifier(ModelBase):
    """
    Baseline model based on Linguistic features + Catboost classifier
    """

    name = "Linguistic features classifier"
    allows_classification: bool = True

    def __init__(self, params: Dict = {}):
        super().__init__(params)
        self.model = None
        self.searched_features = [f['key'] for f in lftk.search_features(language="general")]
        self.spacy_pipelines = {lang: spacy.load(SPACY_DICT[lang]) for lang in tqdm(SPACY_DICT.keys())}

    def fit(self, trainset: WikiDataset, **kwargs) -> None:
        """
        Method used for model training
        Not needed in the baseline model
        """
        train_features, labels = self._prepare_features(trainset)
        # Building classifier model:
        train_data = Pool(
            data=pd.DataFrame(train_features),
            label=labels
        )
        self.model = CatBoostClassifier(iterations=5000, metric_period=500, verbose=True, learning_rate=0.01)
        self.model.fit(train_data, plot=False)

    def predict(self, testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Model used for prediction using trained model
        """
        # Calculate test features:
        test_features, _ = self._prepare_features(testset, train_mode=False)
        # Calculate test scores:
        scores = self.model.predict_proba(
            [[features_sample[k] for k in self.model.feature_names_] for features_sample in test_features]
        )[:, 1]

        # Assigning scores to testset samples:
        i = 0
        for file in tqdm(testset.files):
            for pair in testset.data[file]:
                for sample in pair:
                    sample.score = scores[i]
                    i += 1
        return testset

    def _prepare_features(self, dataset: WikiDataset, train_mode: bool = True):
        features = []
        labels = []
        for file in tqdm(dataset.files):
            # Collect all texts:
            texts = []
            languages = []
            for pair in dataset.data[file]:
                for label, sample in enumerate(pair):
                    text = " ".join(sample.sentences)
                    if len(text) <= 10:
                        text = "Text is not full enough to be processed"
                    texts.append(text)
                    languages.append(sample.lang)
                    if train_mode:
                        labels.append(label)
            assert len(set(languages)) == 1
            spacy_language = list(set(languages))[0]

            # Process texts with spacy + lftk
            nlp = self.spacy_pipelines.get(spacy_language, self.spacy_pipelines[SPACY_DEFAULT_MODEL])
            file_features = []
            for i in tqdm(range(0, len(texts), FEATURES_BATCH_SIZE)):
                docs = list(nlp.pipe(texts[i:i + FEATURES_BATCH_SIZE]))
                file_features += lftk.Extractor(docs=docs).extract(self.searched_features)
            features += file_features
        return features, labels
