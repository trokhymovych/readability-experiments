import copy
import gc
import itertools
import time

from collections import defaultdict
from typing import Dict

import joblib
import numpy as np
import pandas as pd
import torch
import torch.nn as nn

from fuzzywuzzy import fuzz
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm
from transformers import AdamW, AutoModel, AutoTokenizer

from readability_experiments.constants import (
    MODELS_PATH,
    SAMPLE_TEST_SIZE,
    TRRankerSentenceBased_CONFIG,
    TRRankerTextBased_CONFIG,
)
from readability_experiments.entities import WikiDataset
from readability_experiments.models.base import ModelBase


class ReadabilityDataset(Dataset):
    def __init__(self, df, tokenizer, max_length):
        self.df = df
        self.max_len = max_length
        self.tokenizer = tokenizer
        self.hard_text = df['hard_text'].values
        self.easy_text = df['easy_text'].values

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        hard_text = self.hard_text[index]
        easy_text = self.easy_text[index]
        inputs_hard_text = self.tokenizer.encode_plus(
            hard_text,
            truncation=True,
            add_special_tokens=True,
            max_length=self.max_len,
            padding='max_length'
        )
        inputs_easy_text = self.tokenizer.encode_plus(
            easy_text,
            truncation=True,
            add_special_tokens=True,
            max_length=self.max_len,
            padding='max_length'
        )
        target = 1

        hard_text_ids = inputs_hard_text['input_ids']
        hard_text_mask = inputs_hard_text['attention_mask']

        easy_text_ids = inputs_easy_text['input_ids']
        easy_text_mask = inputs_easy_text['attention_mask']

        return {
            'hard_text_ids': torch.tensor(hard_text_ids, dtype=torch.long),
            'hard_text_mask': torch.tensor(hard_text_mask, dtype=torch.long),
            'easy_text_ids': torch.tensor(easy_text_ids, dtype=torch.long),
            'easy_text_mask': torch.tensor(easy_text_mask, dtype=torch.long),
            'target': torch.tensor(target, dtype=torch.long)
        }


class ReadabilityModel(nn.Module):
    def __init__(self, model_name):
        super(ReadabilityModel, self).__init__()
        self.model = AutoModel.from_pretrained(model_name)
        self.drop = nn.Dropout(p=0.2)
        self.fc = nn.Linear(768, 1)

    def forward(self, ids, mask):
        out = self.model(input_ids=ids, attention_mask=mask,
                         output_hidden_states=False)
        out = self.drop(out[1])
        outputs = self.fc(out)

        return outputs


class TRRankerTextBased(ModelBase):
    """
    Transformer ranker model based on the whole text
    """

    name = "Transformer based ranker"
    allows_classification: bool = False

    def __init__(self, params: Dict = TRRankerTextBased_CONFIG):
        super().__init__(params)
        # model initialization:
        self.params = params
        self.model_checkpoint = self.params["model_name"]
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_checkpoint)
        self.sentence_limit = self.params["n_sentences"]
        self.model_prefix = params.get("model_prefix", "full_text")
        self.model = None
        if torch.cuda.is_available():
            self.device = 0
        else:
            self.device = "cpu"

    def fit(self, trainset: WikiDataset, **kwargs) -> None:
        """
        Method used for model training. Parameters are hardcoded.
        """
        def criterion(outputs1, outputs2, targets):
            return nn.MarginRankingLoss(margin=self.params['margin'])(outputs1, outputs2, targets)

        def train_one_epoch(model, optimizer, scheduler, dataloader, device, epoch):
            model.train()
            dataset_size = 0
            running_loss = 0.0
            bar = tqdm(enumerate(dataloader), total=len(dataloader))
            for step, data in bar:
                hard_text_ids = data['hard_text_ids'].to(device, dtype=torch.long)
                hard_text_mask = data['hard_text_mask'].to(device, dtype=torch.long)
                easy_text_ids = data['easy_text_ids'].to(device, dtype=torch.long)
                easy_text_mask = data['easy_text_mask'].to(device, dtype=torch.long)
                targets = data['target'].to(device, dtype=torch.long)
                batch_size = hard_text_ids.size(0)
                hard_text_outputs = model(hard_text_ids, hard_text_mask)
                easy_text_outputs = model(easy_text_ids, easy_text_mask)
                loss = criterion(hard_text_outputs, easy_text_outputs, targets)
                loss = loss / self.params['n_accumulate']
                loss.backward()
                if (step + 1) % self.params['n_accumulate'] == 0:
                    optimizer.step()
                    # zero the parameter gradients
                    optimizer.zero_grad()
                    if scheduler is not None:
                        scheduler.step()
                running_loss += (loss.item() * batch_size)
                dataset_size += batch_size
                epoch_loss = running_loss / dataset_size
                bar.set_postfix(Epoch=epoch, Train_Loss=epoch_loss,
                                LR=optimizer.param_groups[0]['lr'])
            gc.collect()
            return epoch_loss

        @torch.no_grad()
        def valid_one_epoch(model, optimizer, dataloader, device, epoch):
            model.eval()
            dataset_size = 0
            running_loss = 0.0
            bar = tqdm(enumerate(dataloader), total=len(dataloader))
            for _, data in bar:
                hard_text_ids = data['hard_text_ids'].to(device, dtype=torch.long)
                hard_text_mask = data['hard_text_mask'].to(device, dtype=torch.long)
                easy_text_ids = data['easy_text_ids'].to(device, dtype=torch.long)
                easy_text_mask = data['easy_text_mask'].to(device, dtype=torch.long)
                targets = data['target'].to(device, dtype=torch.long)
                batch_size = hard_text_ids.size(0)
                hard_text_outputs = model(hard_text_ids, hard_text_mask)
                easy_text_outputs = model(easy_text_ids, easy_text_mask)
                loss = criterion(hard_text_outputs, easy_text_outputs, targets)
                running_loss += (loss.item() * batch_size)
                dataset_size += batch_size
                epoch_loss = running_loss / dataset_size
                bar.set_postfix(Epoch=epoch, Valid_Loss=epoch_loss,
                                LR=optimizer.param_groups[0]['lr'])
            gc.collect()
            return epoch_loss

        def run_training(model, train_loader, valid_loader, optimizer, scheduler, device, num_epochs, model_name):
            start = time.time()
            best_model_wts = copy.deepcopy(model.state_dict())
            best_epoch_loss = np.inf
            history = defaultdict(list)
            for epoch in range(1, num_epochs + 1):
                gc.collect()
                train_epoch_loss = train_one_epoch(model, optimizer, scheduler,
                                                   dataloader=train_loader,
                                                   device=device, epoch=epoch)

                val_epoch_loss = valid_one_epoch(model, optimizer, valid_loader, device=device,
                                                 epoch=epoch)

                history['Train Loss'].append(train_epoch_loss)
                history['Valid Loss'].append(val_epoch_loss)
                # Log the metrics
                print({"Train Loss": train_epoch_loss})
                print({"Valid Loss": val_epoch_loss})
                # deep copy the model
                if val_epoch_loss <= best_epoch_loss:
                    print(f"Validation Loss Improved ({best_epoch_loss} >>> {val_epoch_loss})")
                    best_epoch_loss = val_epoch_loss
                    best_model_wts = copy.deepcopy(model.state_dict())
                    PATH = f"{MODELS_PATH}{model_name}.bin"
                    torch.save(model.state_dict(), PATH)
                    # Save a model file from the current directory
                    print("Model Saved")
                PATH = f"{MODELS_PATH}{model_name}_e{epoch}_{train_epoch_loss}_{val_epoch_loss}.bin"
                torch.save(model.state_dict(), PATH)
                print()
            end = time.time()
            time_elapsed = end - start
            print(f'Time taken to train: {time_elapsed}s')
            print("Best Loss: {:.4f}".format(best_epoch_loss))
            # load best model weights
            model.load_state_dict(best_model_wts)
            return model, history

        def fetch_scheduler(optimizer):
            if self.params['scheduler'] == 'CosineAnnealingLR':
                scheduler = lr_scheduler.CosineAnnealingLR(optimizer, T_max=self.params['T_max'],
                                                           eta_min=self.params['min_lr'])
            else:
                raise NotImplementedError
            return scheduler

        train_loader, valid_loader = self._prepare_data(trainset)
        model = ReadabilityModel(self.model_checkpoint)
        model.to(self.device)

        # Define Optimizer and Scheduler
        optimizer = AdamW(
            model.parameters(),
            lr=self.params['learning_rate'],
            weight_decay=self.params['weight_decay']
        )

        scheduler = fetch_scheduler(optimizer)
        self.model, _ = run_training(
            model, train_loader, valid_loader, optimizer, scheduler,
            device=self.device, num_epochs=self.params['epochs'], model_name=self.model_prefix
        )
        del model, train_loader, valid_loader
        gc.collect()

    def predict(self, testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Method used for batch predict scores for the whole dataset
        """

        texts, sample_ids = [], []
        sample_id = 0
        for file in testset.files:
            for pair in testset.data[file]:
                for sample in pair:
                    texts.append(" ".join(sample.sentences[:self.sentence_limit]))
                    sample_ids.append(sample_id)
                    sample_id += 1

        # Sorting sentences by the length to make the batch processing more efficient:
        texts_length = [len(t) for t in texts]
        ids = np.argsort(texts_length)[::-1]
        texts_sorted = [texts[i] for i in ids]
        sample_ids_sorted = [sample_ids[i] for i in ids]

        self.model.eval()
        with torch.no_grad():
            predicted_scores = []
            for i in tqdm(range(0, len(texts_sorted), self.params["valid_batch_size"])):
                encoded_input = self.tokenizer(
                    texts_sorted[i: i + self.params["valid_batch_size"]],
                    truncation=True,
                    add_special_tokens=True,
                    max_length=self.params["max_length"],
                    padding='max_length',
                    return_tensors='pt'
                )
                encoded_input = encoded_input.to(self.device)
                predicted_scores += list(
                    self.model(encoded_input["input_ids"], encoded_input["attention_mask"]).cpu().flatten().numpy())

        tmp_df = pd.DataFrame({
            "sample_id": sample_ids_sorted,
            "score": predicted_scores
        })
        tmp_df.index = tmp_df["sample_id"]
        sample_id_to_score = tmp_df.to_dict("index")

        # Assigning scores to the testset
        sample_id = 0
        for file in testset.files:
            for sample_pair in testset.data[file]:
                for sample in sample_pair:
                    sample.score = sample_id_to_score[sample_id]["score"]
                    sample_id += 1
        return testset

    def _prepare_data(self, trainset: WikiDataset):
        """
        Method that process the training dataset to use it for training
        :param trainset:
        :return:
        """
        easy_texts, hard_texts = [], []
        for file in trainset.files:
            for pair in trainset.data[file]:
                easy_texts.append(" ".join(pair[0].sentences[:self.sentence_limit]))
                hard_texts.append(" ".join(pair[1].sentences[:self.sentence_limit]))
        data = pd.DataFrame({"hard_text": hard_texts, "easy_text": easy_texts})
        all_ids = np.arange(len(data))
        test_ids = np.random.choice(all_ids, size=int(len(data) * SAMPLE_TEST_SIZE), replace=False)
        train_ids = list(set(all_ids) - set(test_ids))
        df_train = data.loc[train_ids].reset_index(drop=True)
        df_valid = data.loc[test_ids].reset_index(drop=True)
        train_dataset = ReadabilityDataset(
            df_train, tokenizer=self.tokenizer, max_length=self.params['max_length']
        )
        valid_dataset = ReadabilityDataset(
            df_valid, tokenizer=self.tokenizer, max_length=self.params['max_length']
        )
        train_loader = DataLoader(train_dataset, batch_size=self.params['train_batch_size'],
                                  num_workers=1, shuffle=True, pin_memory=True, drop_last=True)
        valid_loader = DataLoader(valid_dataset, batch_size=self.params['valid_batch_size'],
                                  num_workers=1, shuffle=False, pin_memory=True)
        return train_loader, valid_loader

    def save(self, filename):
        # Move model to CPU before saving
        self.model.to("cpu")
        # Delete the tokenizer to save space
        del self.tokenizer
        # Use joblib to save the class method
        joblib.dump(self, filename)
        # Move model back to GPU after saving (if GPU is available)
        if torch.cuda.is_available():
            self.model.to(0)
        # Reload the tokenizer
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_checkpoint)

    @classmethod
    def load(cls, filename):
        # Use joblib to load the class method
        loaded_instance = joblib.load(filename)
        # Move model back to GPU after loading (if GPU is available)
        if torch.cuda.is_available():
            loaded_instance.model.to(0)
        # Reload the tokenizer
        loaded_instance.tokenizer = AutoTokenizer.from_pretrained(loaded_instance.model_checkpoint)
        return loaded_instance


class TRRankerSentenceBased(TRRankerTextBased):
    """
    Transformer ranker model based on the sentences
    """

    name = "Transformer based ranker (sentence)"
    allows_classification: bool = False

    def __init__(self, params: Dict = TRRankerSentenceBased_CONFIG):
        super().__init__(params)

    def predict(self, testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Method used for batch predict scores for the whole dataset
        """
        texts, sample_ids, sentence_ids = [], [], []
        sample_id = 0
        for file in testset.files:
            for pair in testset.data[file]:
                for sample in pair:
                    sentence_id = 0
                    for sentence in sample.sentences:
                        texts.append(sentence)
                        sample_ids.append(sample_id)
                        sentence_ids.append(sentence_id)
                        sentence_id += 1
                    sample_id += 1

        # Sorting sentences by the length to make the batch processing more efficient:
        texts_length = [len(t) for t in texts]
        ids = np.argsort(texts_length)[::-1]
        texts_sorted = [texts[i] for i in ids]
        sample_ids_sorted = [sample_ids[i] for i in ids]
        sentence_ids_sorted = [sentence_ids[i] for i in ids]

        self.model.eval()
        with torch.no_grad():
            predicted_scores = []
            for i in tqdm(range(0, len(texts_sorted), self.params["valid_batch_size"])):
                encoded_input = self.tokenizer(
                    texts_sorted[i: i + self.params["valid_batch_size"]],
                    truncation=True,
                    add_special_tokens=True,
                    max_length=self.params["max_length"],
                    padding='max_length',
                    return_tensors='pt'
                )
                encoded_input = encoded_input.to(self.device)
                predicted_scores += list(
                    self.model(encoded_input["input_ids"], encoded_input["attention_mask"]).cpu().flatten().numpy())

        tmp_df = pd.DataFrame({
            "sample_id": sample_ids_sorted,
            "sentence_id": sentence_ids_sorted,
            "score": predicted_scores
        }).sort_values("sentence_id")

        sample_id_to_score = tmp_df.groupby("sample_id")["score"].mean().to_dict()
        sample_id_to_sentence_scores = tmp_df.groupby("sample_id")["score"].apply(list).to_dict()

        # Assigning scores to the testset
        sample_id = 0
        for file in testset.files:
            for sample_pair in testset.data[file]:
                for sample in sample_pair:
                    sample.score = sample_id_to_score.get(sample_id, -10)
                    sample.sentence_scores = sample_id_to_sentence_scores.get(sample_id, [])
                    sample_id += 1
        return testset

    def _prepare_data(self, trainset: WikiDataset):
        """
        Method that process the training dataset to use it for training
        :param trainset:
        :return:
        """
        easy_texts, hard_texts = [], []
        for file in trainset.files:
            for pair in trainset.data[file]:
                lines_easy = pair[0].sentences
                lines_hard = pair[1].sentences
                seen = set()
                easy_line_passed, hard_line_passed = [], []
                for easy_line, hard_line in itertools.product(lines_easy, lines_hard):
                    if easy_line not in seen and hard_line not in seen:
                        similarity = fuzz.ratio(easy_line, hard_line)
                        if similarity > self.params["fuzz_sim_min"]:
                            easy_line_passed.append(easy_line)
                            hard_line_passed.append(hard_line)
                            seen.update((easy_line, hard_line))
                easy_texts += easy_line_passed
                hard_texts += hard_line_passed

        data = pd.DataFrame({"hard_text": hard_texts, "easy_text": easy_texts})
        all_ids = np.arange(len(data))
        test_ids = np.random.choice(all_ids, size=int(len(data) * SAMPLE_TEST_SIZE), replace=False)
        train_ids = list(set(all_ids) - set(test_ids))
        df_train = data.loc[train_ids].reset_index(drop=True)
        df_valid = data.loc[test_ids].reset_index(drop=True)
        train_dataset = ReadabilityDataset(
            df_train, tokenizer=self.tokenizer, max_length=self.params['max_length']
        )
        valid_dataset = ReadabilityDataset(
            df_valid, tokenizer=self.tokenizer, max_length=self.params['max_length']
        )
        train_loader = DataLoader(train_dataset, batch_size=self.params['train_batch_size'],
                                  num_workers=1, shuffle=True, pin_memory=True, drop_last=True)
        valid_loader = DataLoader(valid_dataset, batch_size=self.params['valid_batch_size'],
                                  num_workers=1, shuffle=False, pin_memory=True)
        return train_loader, valid_loader
