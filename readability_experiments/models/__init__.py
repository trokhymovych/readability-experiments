from readability_experiments.models.baseline import Baseline
from readability_experiments.models.fk_baseline import FKBaseline
from readability_experiments.models.lf_classifier import LFClassifier
from readability_experiments.models.lf_ranker import LFRanker
from readability_experiments.models.transformer_classifier import TRClassifier
from readability_experiments.models.transformer_ranker import (
    TRRankerSentenceBased,
    TRRankerTextBased,
)


models = [
    Baseline,
    FKBaseline,
    LFClassifier,
    LFRanker,
    TRClassifier,
    TRRankerTextBased,
    TRRankerSentenceBased,
]
