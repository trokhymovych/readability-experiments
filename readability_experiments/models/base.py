from abc import ABC, abstractmethod
from typing import Dict, TypeVar

import joblib

from readability_experiments.entities import WikiDataset


T = TypeVar('T', bound='ModelBase')


class ModelBase(ABC):
    """
    Abstract method for model creation
    """

    name: str = "Readability model abstract class"
    allows_classification: bool = False

    def __init__(self, params: Dict = {}):
        self.params = params

    @abstractmethod
    def fit(self, trainset: WikiDataset) -> None:
        """
        Method used for model training
        """
        raise NotImplementedError

    @abstractmethod
    def predict(self, testset: WikiDataset) -> WikiDataset:
        """
        Model used for prediction using trained model
        """
        raise NotImplementedError

    def save(self, filename):
        # Use joblib to save the class method
        joblib.dump(self, filename)

    @classmethod
    def load(cls, filename):
        # Use joblib to load the class method
        loaded_instance = joblib.load(filename)
        return loaded_instance
