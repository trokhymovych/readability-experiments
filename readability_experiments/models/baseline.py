from tqdm import tqdm

from readability_experiments.entities import WikiDataset
from readability_experiments.models.base import ModelBase


class Baseline(ModelBase):
    """
    Baseline model that simply predict the score based on the number of sentences in text
    """

    name = "Text length baseline"
    allows_classification: bool = False

    def fit(self, trainset: WikiDataset) -> None:
        """
        Method used for model training
        Not needed in the baseline model
        """
        pass

    @staticmethod
    def predict(testset: WikiDataset, **kwargs) -> WikiDataset:
        """
        Model used for prediction using trained model
        """
        for file in tqdm(testset.files):
            for pair in testset.data[file]:
                for sample in pair:
                    sample.score = len(sample.sentences)
        return testset
