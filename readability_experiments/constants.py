DATA_PATH = "../data/wikireadability/"
METRICS_PATH = "../data/metrics/"
MODELS_PATH = "../data/models/"
SPACY_DICT = {
    "fr": "fr_core_news_sm",
    "en": "en_core_web_sm",
    "pt": "pt_core_news_sm",
    "ca": "ca_core_news_sm",
    "es": "es_core_news_sm",
    "de": "de_core_news_sm",
    "nl": "nl_core_news_sm",
    "it": "it_core_news_sm",
    "ru": "ru_core_news_sm",
    "el": "el_core_news_sm"
}
SPACY_DEFAULT_MODEL = "en"
TEXTSTATS_SUPPORTED_LANGUAGES = ["en", "de", "es", "fr", "it", "nl", "pl", "ru"]
TEXTSTATS_DEFAULT_LANGUAGE = "en"
FEATURES_BATCH_SIZE = 500
ACCURACY_TABLE_PATH = '../docs/accuracy.md'
RANKING_ACCURACY_TABLE_PATH = '../docs/ranking_accuracy.md'
RANDOM_SEED = 42
TRAIN_SIZE = 0.8
TEST_SIZE = 0.2
DEFAULT_TRAIN_FILE = "simplewiki-en_sentences.bz2"

BASE_MODEL = "bert-base-multilingual-cased"
MAX_LENGTH = 512
SAMPLE_TEST_SIZE = 0.01
N_EPOCHS = 5
N_SENTENCES = 100

TRRankerTextBased_CONFIG = {
    "epochs": 3,
    "model_name": "Peltarion/xlm-roberta-longformer-base-4096",
    "train_batch_size": 1,
    "valid_batch_size": 4,
    "max_length": 1500,
    "learning_rate": 1e-5,
    "scheduler": 'CosineAnnealingLR',
    "min_lr": 1e-7,
    "T_max": 256,
    "weight_decay": 1e-7,
    "n_accumulate": 1,
    "num_classes": 1,
    "margin": 0.5,
    "n_sentences": 100,
    "model_prefix": "full_text"
}

TRRankerSentenceBased_CONFIG = {
    "epochs": 3,
    "model_name": "bert-base-multilingual-cased",
    "train_batch_size": 16,
    "valid_batch_size": 64,
    "max_length": 256,
    "learning_rate": 1e-5,
    "scheduler": 'CosineAnnealingLR',
    "min_lr": 1e-7,
    "T_max": 256,
    "weight_decay": 1e-7,
    "n_accumulate": 1,
    "num_classes": 1,
    "margin": 0.5,
    "n_sentences": 100,
    "fuzz_sim_min": 30,
    "model_prefix": "sentence_text"
}
