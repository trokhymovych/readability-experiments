import bz2
import glob
import json

from typing import List, Tuple

import numpy as np

from tqdm import tqdm

from readability_experiments.constants import (
    DATA_PATH,
    DEFAULT_TRAIN_FILE,
    RANDOM_SEED,
    TRAIN_SIZE,
)
from readability_experiments.entities import Difficulty, WikiDataset, WikiPage
from readability_experiments.utils import extract_language_code


class DataLoaderBase:
    """
    Class that implements data loading and preparation for the wikireadablity dataset
    """
    def __init__(self, data_path: str = DATA_PATH) -> None:
        self.testsets = None
        self.trainsets = None
        self.data_path = data_path
        self.data = {}
        self.data_files_names = []

    def load_data(self, split_strategy: str = "simple_only"):
        """
        Method that loads data for further processing
        :return: Data for further processing
        """
        data_files = self._get_data_files()
        for file in tqdm(data_files):
            file_name = file.split("/")[-1]
            self.data[file_name] = self._load_data_file(file)
        self.data_files_names = list(self.data.keys())

        if split_strategy == "simple_only":
            self.trainsets, self.testsets = self.train_test_split_simple()
        elif split_strategy == "no_split":
            self.trainsets = WikiDataset(files=self.data_files_names, data=self.data)
        else:
            raise NotImplementedError

    def _get_data_files(self) -> List[str]:
        """
        Method that loads looks for all the data files in the directory
        :return:
        """
        files = []
        for name in glob.glob(self.data_path + "*"):
            files.append(name)
        return files

    @staticmethod
    def _load_data_file(file_path: str) -> List[Tuple[WikiPage, WikiPage]]:
        """
        Method that loads the data given the path in the standardised form
        :param file_path: path to the file to load
        :return: data in form of list of tuples
        """
        data = []
        language = extract_language_code(file_path)
        with bz2.open(file_path, "rt") as fin:
            for line in fin:
                dict_in = json.loads(line)
                hard_s = dict_in[Difficulty.hard.value]
                hard_s["difficulty"] = Difficulty.hard
                hard_s["lang"] = language
                easy_s = dict_in[Difficulty.easy.value]
                easy_s["difficulty"] = Difficulty.easy
                easy_s["lang"] = language
                data.append((WikiPage(**easy_s), WikiPage(**hard_s)))

        return data

    def train_test_split_simple(self) -> (WikiDataset, WikiDataset):
        """
        Method that implements a basic splitting strategy for wikireadability dataset
        Using only part of simplewiki-en_sentences for training. All the other data goes to testing.
        :return: training and testing part of data
        """
        print("Length of data: ", len(self.data[DEFAULT_TRAIN_FILE]))
        idx_list = np.arange(len(self.data[DEFAULT_TRAIN_FILE]))

        # select train  and test idx for DEFAULT_TRAIN_FILE
        np.random.seed(RANDOM_SEED)
        train_size = int(len(self.data[DEFAULT_TRAIN_FILE]) * TRAIN_SIZE)
        train_idx = np.random.choice(idx_list, size=train_size, replace=False)
        test_idx = list(set(idx_list) - set(train_idx))
        print(f"Train size: {len(train_idx)}. Test size: {len(test_idx)}")

        # split dataset in parts:
        trainsets_data = {DEFAULT_TRAIN_FILE: [self.data[DEFAULT_TRAIN_FILE][i] for i in train_idx]}
        testsets_data = {DEFAULT_TRAIN_FILE: [self.data[DEFAULT_TRAIN_FILE][i] for i in test_idx]}
        for name in self.data_files_names:
            if name == DEFAULT_TRAIN_FILE:
                pass
            else:
                testsets_data[name] = self.data[name]

        trainsets = WikiDataset(files=[DEFAULT_TRAIN_FILE], data=trainsets_data)
        testsets = WikiDataset(files=self.data_files_names, data=testsets_data)
        return trainsets, testsets
