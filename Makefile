lint: ## Lint whole python project
	@echo "Run linter readability_experiments"
	flake8 readability_experiments/
	isort --check-only --diff --stdout .
	@echo "Done"

format: ## Format python code
	isort .

install: ## Create virtual environment and setup requirements
	@echo "Setup poetry virtual environment"
	poetry install
	@echo "Done"

activate_virtual_env: ## Activate virtual environment
	@echo "Activating poetry virtual environment"
	poetry shell
	@echo "Done"

ci_lint: ## Lint whole python package in CI runner
	poetry run flake8 readability_experiments/
	poetry run isort --check-only --diff --stdout .
	poetry run mypy readability_experiments/

