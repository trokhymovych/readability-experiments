import pathlib
import sys
import time

from dataclasses import dataclass
from typing import Any, List

import joblib
import numpy as np
import pandas as pd
import torch
import torch.nn as nn

from sklearn.linear_model import LinearRegression
from tqdm import tqdm
from transformers import AutoModel, AutoTokenizer  # type: ignore


sys.path.insert(0, '..')
from readability_experiments.data_loader import DataLoaderBase


torch.set_num_threads(1)


class ReadabilityModel(nn.Module):
    def __init__(self, model_name: str) -> None:
        super(ReadabilityModel, self).__init__()
        self.model = AutoModel.from_pretrained(model_name)
        self.drop = nn.Dropout(p=0.2)
        self.fc = nn.Linear(768, 1)

    def forward(self, ids: Any, mask: Any) -> Any:
        out = self.model(input_ids=ids, attention_mask=mask,
                         output_hidden_states=False)
        out = self.drop(out[1])
        outputs = self.fc(out)
        return outputs
    

# Build the model class
@dataclass
class MultilingualTextReadabilityModel:
    model_version: int
    tokenizer: AutoTokenizer
    scorer: ReadabilityModel
    fk_scorer: LinearRegression
    supported_wikis: List[str] 


def predict_text_scores(
    model: ReadabilityModel,
    tokenizer: AutoTokenizer,
    text: str,
    truncation: bool = True,
    max_length: int = 1500,
    mode: str = "cpu"
) -> float:
    """
    Method that gets the scores for list of sentences using model (transformer pipeline)
    """
    with torch.no_grad():
        encoded_input = tokenizer(
                text,
                truncation=truncation,
                add_special_tokens=True,
                max_length=max_length,
                padding=False,
                return_tensors='pt'
            )
        if mode == "cpu":
            encoded_input = encoded_input.to("cpu")
            model = model.to("cpu")
            predicted_score = model(encoded_input["input_ids"], encoded_input["attention_mask"]).flatten().numpy()[0]
        else:
            if next(model.parameters()).device == "cpu":
                predicted_score = model(encoded_input["input_ids"], encoded_input["attention_mask"]).flatten().numpy()[0]
            else:
                encoded_input = encoded_input.to("cuda")
                model = model.to("cuda")
                predicted_score = model(encoded_input["input_ids"], encoded_input["attention_mask"]).cpu().flatten().numpy()[0]
    return predicted_score

def load_model(model_path: pathlib.Path) -> MultilingualTextReadabilityModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].MultilingualTextReadabilityModel = MultilingualTextReadabilityModel  # type: ignore  # noqa: E501
    sys.modules["__main__"].ReadabilityModel = ReadabilityModel  # type: ignore  # noqa: E501

    with open(model_path, "rb") as f:
        model: MultilingualTextReadabilityModel = joblib.load(f)
    return model


# main script:
if __name__ == "__main__":
    # Loading model
    model = load_model(pathlib.Path("../data/models/multilingual_text_readability_model_v4.bin"))
    model.scorer.eval()

    # Loading texts: 
    loader = DataLoaderBase("../data/wikireadability/")
    loader.load_data()
    texts = []
    titles = []
    difficulties = []
    lang = []
    for file in tqdm(loader.testsets.files):
        for pair in loader.testsets.data[file]:
            for sample in pair:
                texts.append(" ".join(sample.sentences))
                titles.append(sample.title)
                difficulties.append(sample.difficulty)
                lang.append(sample.lang)
    # Randomly select 1000 texts (fix seed):
    np.random.seed(42)
    ids = np.random.choice(np.arange(len(lang)), 1000, replace=False)
    texts = [texts[i] for i in ids]
    titles = [titles[i] for i in ids]
    difficulties = [difficulties[i] for i in ids]
    lang = [lang[i] for i in ids]

    # Predicting scores and measuring time:
    scores = []
    times = []
    for text in tqdm(texts):
        start_time = time.time()
        text_score = predict_text_scores(
            model=model.scorer,
            tokenizer=model.tokenizer,
            text=text
        )
        end_time = time.time()
        times.append(end_time - start_time)
        scores.append(text_score)
    print(f"Median time: {np.median(times)}")
    print(f"75% percentile time: {np.percentile(times, 75)}")
    print(f"95% percentile time: {np.percentile(times, 95)}")

    df = pd.DataFrame({"title": titles, "difficulty": difficulties, "lang": lang, "score": scores, "time": times})
    df.to_csv("../data/time_scores.csv", index=False)
