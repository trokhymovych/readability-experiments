import sys


sys.path.insert(0, '..')

from readability_experiments.constants import MODELS_PATH, RANDOM_SEED
from readability_experiments.data_loader import DataLoaderBase
from readability_experiments.metrics import Evaluator
from readability_experiments.models import models
from readability_experiments.utils import set_seed


set_seed(RANDOM_SEED)
loader = DataLoaderBase("../data/wikireadability/")
loader.load_data()

models = [m.load(MODELS_PATH + m.name.replace(" ", "_") + ".mod") for m in models]
evaluator = Evaluator(models)
evaluator.get_models_metrics(loader.testsets)
