import glob
import os
import pickle
import sys

import numpy as np


sys.path.insert(0, '..')
import time

from readability_experiments.constants import MODELS_PATH
from readability_experiments.entities import WikiDataset, WikiPage
from readability_experiments.models import models


strings_to_remove = [
    "data/one-stop-english-corpus/Int-Txt/",
    "data/one-stop-english-corpus/Ele-Txt/",
    "data/one-stop-english-corpus/Adv-Txt/",
    "-adv.txt",
    "-ele.txt",
    "-int.txt",
]

levels_dict = {
    "-adv.txt": "advanced",
    "-ele.txt": "elementary",
    "-int.txt": "intermediate",
}
text_data = {}

files = []
for file in glob.glob("../data/one-stop-english-corpus/*/*"):
    files.append(file)

for file in files:
    if ".txt" not in file:
        continue
    # level 
    level = None
    for k, v in levels_dict.items():
        if k in file:
            level = v
    # name
    name = file
    for s in strings_to_remove:
        name = name.replace(s, "")
    
    # content
    file = open(file, "r")
    content = [l for l in file.readlines() if l != '\n' and l != 'Intermediate \n']
    file.close()
    current_dict = text_data.get(name, {})
    current_dict[level] = content
    text_data[name] = current_dict
print(f"Number of texts triplets: {len(text_data)}")

levels = ["elementary", "intermediate", "advanced"]
files = ["OneStopEnglish"]

data_one_stop_english = []
for article_name, article_dict in text_data.items():
    slug = []
    for level in levels:
        slug.append(
            WikiPage(
                title=article_name,
                difficulty="easy",
                sentences=article_dict[level],
                lang="en",
            )
        )
    data_one_stop_english.append(slug)
data = {files[0]: data_one_stop_english}

data_one_stop_english = WikiDataset(files=files, data=data)

langs = ["fr", "en", "frsimple", "ensimple"]
text_data = {
    "fr": [],
    "en": [],
    "frsimple": [],
    "ensimple": [],
}

for lang in langs:
    print(f"Language: {lang}")
    files = []
    n_sentence = []
    n_chars = []
    for file_name in glob.glob(f"../data/vikidia-en-fr/Vikidia-EnFr-Parsed/{lang}/*"):
        file = open(file_name, "r")
        content = [l for l in file.readlines() if l != '\n' and l != 'Intermediate \n']
        file.close()
        n_sentence += [len(content)]
        n_chars += [np.sum([len(i) for i in content])]
        text_data[lang].append(
            WikiPage(
                title=file_name.replace(f"../data/vikidia-en-fr/Vikidia-EnFr-Parsed/{lang}/", ""),
                difficulty="easy" if "simple" not in lang else "hard",
                sentences=content,
                lang=lang.replace("simple", ""),
            )
        )
    print("Number of samples: ", len(n_sentence))
    print("avg chars/doc: ", np.mean(n_chars))
    print("avg sentences/doc: ", np.mean(n_sentence))
    print("_"*50)


# Creating needed structure from WikiDataset
files = ["VikidiaEn", "VikidiaFr"]
data = {
    "VikidiaEn": [(text_data["ensimple"][i], text_data["en"][i]) for i in range(len(text_data["en"]))],
    "VikidiaFr": [(text_data["frsimple"][i], text_data["fr"][i]) for i in range(len(text_data["fr"]))],
}
dataset_vikidia = WikiDataset(files=files, data=data)

models = [m.load(MODELS_PATH + m.name.replace(" ", "_") + ".mod") for m in models]
for model in models:
    print(f"Processing {model.name}")
    start_time = time.time()
    processed_dataset_vikidia = model.predict(dataset_vikidia)
    processed_dataset_one_stop_english = model.predict(data_one_stop_english)
    time_elapsed = time.time() - start_time

    # Create directory"../data/external_scores" if it does not exist:
    if not os.path.exists("../data/external_scores"):
        os.makedirs("../data/external_scores")
    # Dumping results
    with open(f"../data/external_scores/processed_dataset_vikidia_{model.name.replace(' ', '_')}.pkl", "wb") as f:
        pickle.dump(processed_dataset_vikidia, f)
    with open(f"../data/external_scores/processed_dataset_one_stop_english_{model.name.replace(' ', '_')}.pkl", "wb") as f:
        pickle.dump(processed_dataset_one_stop_english, f)
