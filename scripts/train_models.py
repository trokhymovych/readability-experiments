import gc
import sys

import torch

from tqdm import tqdm


sys.path.insert(0, '..')

from readability_experiments.constants import MODELS_PATH, RANDOM_SEED
from readability_experiments.data_loader import DataLoaderBase
from readability_experiments.models import models
from readability_experiments.utils import set_seed


set_seed(RANDOM_SEED)
loader = DataLoaderBase("../data/wikireadability/")
loader.load_data()

for model in tqdm(models):
    model_local = model()
    print(f"Training {model_local.name}")
    model_local.fit(loader.trainsets)
    model_local.save(MODELS_PATH + model_local.name.replace(" ", "_") + ".mod")
    del model_local
    torch.cuda.empty_cache()
    gc.collect()
