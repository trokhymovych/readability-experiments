import os
import pickle
import sys

import pandas as pd
import textstat

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score, train_test_split
from tqdm import tqdm


directory = "../data/metrics/"
filename_scores = None
for filename in os.listdir(directory):
    if ("all_scores__Transformer_based_ranker" in filename) and ("sentence" not in filename):
        filename_scores = filename
        break
transformer_scores = directory + filename_scores
print("Loading scores from ", transformer_scores)

sys.path.insert(0, '..')
from readability_experiments.data_loader import DataLoaderBase


loader = DataLoaderBase("../data/wikireadability/")
loader.load_data()

# Calculation of FK score: 
def predict(testset):
    for file in tqdm(testset.files):
        for pair in testset.data[file]:
            for sample in pair:
                sample.score = textstat.flesch_kincaid_grade(" ".join(sample.sentences))
    return testset

predicted_dataset = predict(loader.testsets)

# Loading model predicted scores: 
dataset = pd.read_csv(transformer_scores)
scores_dataset = dataset[dataset['file'] == 'simplewiki-en_sentences.bz2']
# Creating FK score dict:
fk_dict = {}
for pair in predicted_dataset.data["simplewiki-en_sentences.bz2"]:
    for sample in pair:
        fk_dict[(sample.title, sample.difficulty)] = sample.score

# Getting FK scores from dict for scores_dataset:
fk_scores = []
for index, row in scores_dataset.iterrows():
    fk_scores.append(fk_dict[(row['title'], row['difficulty'])])

# Adding FK scores to scores_dataset:
scores_dataset['fk_score'] = fk_scores

# Building a linear regression model to predict FK scores based on model scores:
X = scores_dataset['score'].values.reshape(-1, 1)
y = scores_dataset['fk_score'].values.reshape(-1, 1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
reg = LinearRegression()
reg.fit(X_train, y_train)
y_pred = reg.predict(X_test)

print("RMSE: ", mean_squared_error(y_test, y_pred, squared=False))
score = cross_val_score(reg, X, y, cv=5, scoring="neg_root_mean_squared_error")
print("CV score: ", score.mean())

# Dumping the model:
pickle.dump(reg, open("../data/fk_model.pkl", 'wb'))
