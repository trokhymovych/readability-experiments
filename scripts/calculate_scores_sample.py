import bz2
import glob
import json
import sys
import time

import pandas as pd


sys.path.insert(0, '..')

from readability_experiments.constants import MODELS_PATH
from readability_experiments.entities import WikiDataset, WikiPage
from readability_experiments.models import models


models = [m.load(MODELS_PATH + m.name.replace(" ", "_") + ".mod") for m in [models[-2]]]
model = models[0]
print(model)

def load_data_file_inference(file_path):
    data = []
    with bz2.open(file_path, "rt") as fin:
        for line in fin:
            dict_in = json.loads(line)
            data.append(
                (
                    WikiPage(
                        title=dict_in["page_title"],
                        sentences=dict_in["sentences"],
                        difficulty="easy",
                        lang="unknown"
                    ),
                )
            )

    return data

data_path = "../data/wikireadability_sample/"
files = []
for name in glob.glob(data_path + "*"):
    files.append(name)
data = {f.replace(data_path, ""): load_data_file_inference(f) for f in files}
dataset = WikiDataset(files=[f.replace(data_path, "") for f in files], data=data)
print(files)

start_time = time.time()
processed_dataset = model.predict(dataset)
time_elapsed = time.time() - start_time

files = []
titles = []
scores = []
for file in processed_dataset.files:
    for page in processed_dataset.data[file]:
        files.append(file)
        titles.append(page[0].title)
        scores.append(page[0].score)

df = pd.DataFrame({"file": files, "title": titles, "score": scores})
df.to_csv("../data/wikireadability_sample/results_text_ranker.csv", index=False)
