import subprocess

from typing import List

import requests

from bs4 import BeautifulSoup

from readability_experiments.constants import DATA_PATH


BASE_URL = "https://analytics.wikimedia.org/published/datasets/one-off/mgerlach/readability/data/"


def get_file_links(base_url: str = BASE_URL, ending='.bz2', starting="") -> List[str]:
    # Send a GET request to the URL
    response = requests.get(base_url)
    if response.status_code == 200:
        # Parse the HTML content
        soup = BeautifulSoup(response.content, 'html.parser')
        # Find all links with the .bz extension and not include "sample_" prefix
        if starting=="":
            links = soup.find_all('a', href=lambda href: (href and href.endswith(ending) and not href.startswith("sample_")))
            file_links = [base_url + l.text for l in links]
        else:
            links = soup.find_all('a', href=lambda href: (href and href.endswith(ending) and href.startswith(starting)))
            file_links = [base_url + l.text for l in links]
    else:
        file_links = []
    return file_links


def download_file(file_url: str, path: str = DATA_PATH) -> None:
    subprocess.run(["wget", "-P", path, file_url])


if __name__ == '__main__':
    file_urls = get_file_links()
    for link in file_urls:
        download_file(link)
    
    # downloading info about matching between simple and hard articles
    commons_url = get_file_links(ending=".tsv")
    for link in commons_url:
        download_file(link, path="../data/wikireadability_matching/")
    
    # downloading info about sample articles
    sample_urls = get_file_links(starting="sample_")
    for link in sample_urls:
        download_file(link, path="../data/wikireadability_sample/")
    