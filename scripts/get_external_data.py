import os

import requests


def download_github_folder(repo_url, folder_path, destination_folder):
    api_url = f"{repo_url.rstrip('/')}/contents/{folder_path}"
    response = requests.get(api_url)
    response.raise_for_status()
    os.makedirs(destination_folder, exist_ok=True)
    for item in response.json():
        if item['type'] == 'file':
            file_url = item['download_url']
            file_path = os.path.join(destination_folder, item['name'])
            file_response = requests.get(file_url)
            with open(file_path, 'wb') as file:
                file.write(file_response.content)
            print(f"Downloaded: {item['name']}")

if __name__ == "__main__":
    # Downloading OneStopEnglishCorpus
    paths = ["Int-Txt", "Ele-Txt", "Adv-Txt"]
    repo_url = "https://api.github.com/repos/nishkalavallabhi/OneStopEnglishCorpus"
    for path in paths:
        folder_path = f"Texts-SeparatedByReadingLevel/{path}"
        destination_folder = f"../data/one-stop-english-corpus/{path}"
        try:
            download_github_folder(repo_url, folder_path, destination_folder)
        except Exception as e:
            print(f"Error: {e}")

    # Downloading Vikidia-en-fr dataset
    vikidia_dataset_download_link = "https://zenodo.org/records/6327828/files/Vikidia-EnFr-Parsed.tar.gz"
    vikidia_dataset_destination_folder = "../data/vikidia-en-fr"
    os.makedirs(vikidia_dataset_destination_folder, exist_ok=True)
    os.system(f"wget {vikidia_dataset_download_link} -P {vikidia_dataset_destination_folder}")
    os.system(f"tar -xzf {vikidia_dataset_destination_folder}/Vikidia-EnFr-Parsed.tar.gz -C {vikidia_dataset_destination_folder}")
    os.system(f"rm {vikidia_dataset_destination_folder}/Vikidia-EnFr-Parsed.tar.gz")
