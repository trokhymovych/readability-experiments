import pickle
import sys

from dataclasses import dataclass
from typing import List

import joblib
import torch
import torch.nn as nn

from sklearn.linear_model import LinearRegression
from transformers import AutoModel, AutoTokenizer


sys.path.insert(0, '..')

from readability_experiments.models.transformer_ranker import TRRankerTextBased


MODEL_VERSION = 4


# Define the model class (Copy from the model file)
class ReadabilityModel(nn.Module):
    def __init__(self, model_name):
        super(ReadabilityModel, self).__init__()
        self.model = AutoModel.from_pretrained(model_name)
        self.drop = nn.Dropout(p=0.2)
        self.fc = nn.Linear(768, 1)

    def forward(self, ids, mask):
        out = self.model(input_ids=ids, attention_mask=mask,
                         output_hidden_states=False)
        out = self.drop(out[1])
        outputs = self.fc(out)

        return outputs
    

model = TRRankerTextBased.load("../data/models/Transformer_based_ranker.mod")

# move model.model to cpu
model.model = model.model.to('cpu')

# Save model.model
torch.save(model.model.state_dict(), '../data/models/transformer_based_ranker_model_v1.pth')

# Build model class for inference
rmodel = ReadabilityModel("Peltarion/xlm-roberta-longformer-base-4096")
state_dict = torch.load('../data/models/transformer_based_ranker_model_v1.pth')
rmodel.load_state_dict(state_dict)

# load regression model
reg_model = pickle.load(open("../data/fk_model.pkl", 'rb'))
supported_wikis = [
    'af', 'an', 'ar', 'ast', 'azb', 'az', 'bar', 'ba', 'be', 'bg', 'bn', 'bpy', 'br', 'bs', 'ca', 'ceb', 'ce', 'cs',
    'cv', 'cy', 'da', 'de', 'el', 'en', 'es', 'et', 'eu', 'fa', 'fi', 'fr', 'fy', 'ga', 'gl', 'gu', 'he', 'hi', 'hr',
    'ht', 'hu', 'hy', 'id', 'io', 'is', 'it', 'ja', 'jv', 'ka', 'kk', 'kn', 'ko', 'ky', 'la', 'lb', 'lmo', 'lt', 'lv',
    'mg', 'min', 'mk', 'ml', 'mn', 'mr', 'ms', 'my', 'nds_nl', 'ne', 'new', 'nl', 'nn', 'no', 'oc', 'pa', 'pl', 'pms',
    'pnb', 'pt', 'ro', 'ru', 'scn', 'sco', 'sh', 'sk', 'sl', 'sq', 'sr', 'su', 'sv', 'sw', 'ta', 'te', 'tg', 'th',
    'tl', 'tr', 'tt', 'uk', 'ur', 'uz', 'vi', 'vo', 'war', 'yo', 'zh', 'simple'
]

# Build the model class
@dataclass
class MultilingualTextReadabilityModel:
    model_version: int
    tokenizer: AutoTokenizer
    scorer: ReadabilityModel
    fk_scorer: LinearRegression
    supported_wikis: List[str] 

# Loading the model:
model_instance = MultilingualTextReadabilityModel(
    model_version=MODEL_VERSION, 
    tokenizer=model.tokenizer,
    scorer=rmodel, 
    fk_scorer=reg_model, 
    supported_wikis=supported_wikis
)
binary_model_path = f"../data/models/multilingual_text_readability_model_v{MODEL_VERSION}.bin"
joblib.dump(model_instance, binary_model_path, compress=9)
