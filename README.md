
# Readability Experiments
[![Wikipedia](https://img.shields.io/badge/Wikipedia-%23000000.svg?style=for-the-badge&logo=wikipedia&logoColor=white)](https://meta.wikimedia.org/wiki/Machine_learning_models/Proposed/Multilingual_readability_model_card)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11371932.svg)](https://doi.org/10.5281/zenodo.11371932)

<img align="right" src="imgs/idea.png" alt="drawing" style="width:300px;"/>
The repository includes code and data for the paper "An open multilingual system for scoring readability of Wikipedia."

## Repository structure:
1. Directory `readability_experiments` includes the main code used for data processing, model training, and testing. In particular, `readability_experiments/models` includes implementing all the presented models and baseline logic. `readability_experiments/data_loader.py` contains the implementation of the class used for data loading and preparation.
2. Directory `notebooks` include Jupyter notebooks used to perform additional calculations and visualizations:
    1. `notebooks/01_Basic_EDA.ipynb` -> EDA of the presented dataset
    2. `notebooks/02_External_datasets_EDA.ipynb` -> EDA of the previous benchmark datasets
    3. `notebooks/03_Languages_intersection.ipynb` -> Additional analysis of the articles intersection within languages in the newly presented dataset
    4. `notebooks/04_Visualizations.ipynb`
    5. `notebooks/05_Final_Metrics_and_bootstraping.ipynb`
    6. `notebooks/06_Publishing_model_and_Inference.ipynb` -> Used to publish the model to HuggingFace Hub and show inference examples
3. Directory `scripts` include Python scripts needed to reproduce the experiments, including model training, testing, and additional scores calculation. 
4. Directory `data` includes all the necessary data for the experiments (explained in the next section in more details).


## Data
The data can be accessed at the [following directory](https://analytics.wikimedia.org/published/datasets/one-off/mgerlach/readability/data/).

Also, the data is published in Zenodo: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11371932.svg)](https://doi.org/10.5281/zenodo.11371932)

Alternatively, you can use the script to automatically download all the necessary data for the experiment.
```bash
python get_data.py
```

## Inference: 
 The model is available on the HuggingFace Hub, along with usage examples: [Open Multilingual Text Readability Scoring Model (TRank)](https://huggingface.co/trokhymovych/TRank_readability)


## Experiments: 
All the experiments are implemented with `.py` scripts:

Before running the script, please install dependencies using `make install` and activate the env using `poetry shell` command.

1. Models training:
The script loads the data, performs a train-test split (fixed seed), and sequentially trains models defined in the `readability_experiments/models` The artifacts are stored in `data/models`. It takes ~ four days on 1x AMD Radeon Pro WX 1255 9100 16GB GPU. 
```bash
cd scripts && python train_models.py
```

2. Models testing: 
The script loads the data and models, performs a train-test split (fixed seed, the same as train), and predicts scores for all articles in the testing part. It saves the artifacts in `data/metrics`
```bash
cd scripts && python test_models.py
```

3. Speed benchmarking: 
The script selects the random 1K articles from the test and performs the inference score calculation using the same strategy used while on prod. Evaluates the speed performance of the model. 
```bash
cd scripts && python speed_benchmark.py
```

4. Getting external data: 
Script that is used to download the external data (previous benchmarks) used in our experiments. 
```bash
cd scripts && python get_external_data.py
```

5. Model inference & Scores calculation: 
Scripts were used to calculate scores on the external data (previous benchmarks) and a sample of Wikipedia (for the state of readability definition). 
```bash
cd scripts && python calculate_scores_external.py && python calculate_scores_sample.py
```

6. Binary setup (optional):
Used to create the binary file of the models from the training artifacts. The resulting binary is used for inference in the API. 
```bash
cd scripts && python build_fk_scorer.py && python binary_setup.py
```



## Results
#### Ranking accuracy and corresponding confidence intervals (CI) on different test datasets (zero-shot scenario for all datasets except simplewiki-en). Confidence intervals denote two standard deviations from bootstrapping.
Dataset       |    NS |   ±CI |   FRE |   ±CI |   LFC |   ±CI |   LFR |   ±CI |   LMC |   ±CI |   TRank |   ±CI |   SRank |   ±CI |
|:--------------|------:|------:|------:|------:|------:|------:|------:|------:|------:|------:|--------:|------:|--------:|------:|
| simplewiki-en | 0.543 | 0.007 | 0.868 | 0.005 | 0.937 | 0.003 | 0.945 | 0.003 | 0.965 | 0.002 |   0.976 | 0.002 |   0.972 | 0.002 |
| vikidia-en    | 0.814 | 0.017 | 0.935 | 0.011 | 0.979 | 0.006 | 0.981 | 0.006 | 0.979 | 0.006 |   0.991 | 0.004 |   0.985 | 0.005 |
| vikidia-ca    | 0.782 | 0.054 | 0.906 | 0.038 | 0.94  | 0.031 | 0.932 | 0.033 | 0.936 | 0.032 |   0.962 | 0.025 |   0.936 | 0.032 |
| vikidia-de    | 0.735 | 0.054 | 0.815 | 0.048 | 0.888 | 0.039 | 0.869 | 0.042 | 0.908 | 0.036 |   0.938 | 0.03  |   0.919 | 0.034 |
| vikidia-el    | 0.718 | 0.144 | 0.718 | 0.144 | 0.744 | 0.14  | 0.795 | 0.129 | 0.897 | 0.096 |   0.923 | 0.086 |   0.897 | 0.097 |
| vikidia-es    | 0.573 | 0.023 | 0.842 | 0.017 | 0.883 | 0.015 | 0.892 | 0.014 | 0.879 | 0.015 |   0.911 | 0.013 |   0.909 | 0.013 |
| vikidia-eu    | 0.541 | 0.042 | 0.673 | 0.04  | 0.639 | 0.04  | 0.623 | 0.041 | 0.63  | 0.04  |   0.818 | 0.032 |   0.736 | 0.037 |
| vikidia-fr    | 0.553 | 0.009 | 0.84  | 0.007 | 0.82  | 0.007 | 0.845 | 0.006 | 0.849 | 0.007 |   0.923 | 0.005 |   0.918 | 0.005 |
| vikidia-hy    | 0.394 | 0.045 | 0.594 | 0.045 | 0.534 | 0.045 | 0.598 | 0.044 | 0.637 | 0.044 |   0.802 | 0.036 |   0.761 | 0.039 |
| vikidia-it    | 0.569 | 0.024 | 0.83  | 0.019 | 0.919 | 0.013 | 0.94  | 0.012 | 0.925 | 0.013 |   0.958 | 0.01  |   0.952 | 0.01  |
| vikidia-oc    | 0.667 | 0.273 | 0.667 | 0.271 | 0.75  | 0.25  | 0.667 | 0.27  | 0.917 | 0.159 |   1     | 0     |   0.917 | 0.161 |
| vikidia-pt    | 0.761 | 0.03  | 0.869 | 0.024 | 0.938 | 0.017 | 0.934 | 0.017 | 0.921 | 0.019 |   0.96  | 0.014 |   0.938 | 0.017 |
| vikidia-ru    | 0.728 | 0.08  | 0.608 | 0.087 | 0.736 | 0.078 | 0.776 | 0.074 | 0.736 | 0.079 |   0.88  | 0.058 |   0.76  | 0.077 |
| vikidia-scn   | 0.4   | 0.314 | 0.6   | 0.309 | 0.6   | 0.308 | 0.8   | 0.254 | 0.6   | 0.31  |   0.9   | 0.191 |   1     | 0     |
| klexikon-de   | 0.114 | 0.013 | 0.984 | 0.005 | 0.999 | 0.002 | 0.995 | 0.003 | 0.991 | 0.004 |   0.999 | 0.002 |   0.996 | 0.003 |
| txikipedia-eu | 0.512 | 0.029 | 0.707 | 0.027 | 0.689 | 0.027 | 0.698 | 0.027 | 0.67  | 0.027 |   0.81  | 0.023 |   0.762 | 0.025 |
| wikikids-nl   | 0.427 | 0.009 | 0.795 | 0.007 | 0.831 | 0.007 | 0.834 | 0.007 | 0.85  | 0.007 |   0.897 | 0.006 |   0.907 | 0.005 |

#### Ranking accuracy and corresponding confidence intervals (CI) on previous reference datasets (zero-shot scenario for all datasets)

| Dataset        |    NS |   ±CI |   FRE |   ±CI |   LFC |   ±CI |   LFR |   ±CI |   LMC |   ±CI |   TRank |   ±CI |   SRank |   ±CI |
|:---------------|------:|------:|------:|------:|------:|------:|------:|------:|------:|------:|--------:|------:|--------:|------:|
| VikidiaEn      | 0.966 | 0.005 | 0.948 | 0.006 | 0.888 | 0.008 | 0.946 | 0.006 | 0.965 | 0.005 |   0.984 | 0.003 |   0.98  | 0.004 |
| VikidiaFr      | 0.952 | 0.005 | 0.899 | 0.008 | 0.878 | 0.008 | 0.888 | 0.008 | 0.75  | 0.011 |   0.978 | 0.004 |   0.911 | 0.007 |
| OneStopEnglish | 0.794 | 0.059 | 0.915 | 0.04  | 0.889 | 0.046 | 0.873 | 0.048 | 0.942 | 0.034 |   0.974 | 0.023 |   0.989 | 0.015 |



## Developing
#### Prerequisites
* Install the latest version of [Poetry](https://python-poetry.org/docs/#installation)

#### Setup
After installing the prerequisites and cloning the repo, run the following:
```bash
cd readability-experiments
make install
```

Before doing the commit, please use the following:
- Automated formatting:
```bash
make format
```
- Checking formatting: 
```bash
make lint
```

#### Jupyter: 
To set up a kernel from the poetry environment, run the following: 
```bash
poetry run python -m ipykernel install --user --name poetry_kernel
```

### Acknowledgement: 
- [Jigsaw Starter](https://www.kaggle.com/code/debarshichanda/pytorch-w-b-jigsaw-starter/notebook) by DEBARSHI CHANDA: used while developing `models/transformer_ranker.py`.
- [Github Copilot](https://github.com/features/copilot) was utilized in this project primarily for advanced autocomplete functionality, advancing the plots, and providing suggestions during the development process.